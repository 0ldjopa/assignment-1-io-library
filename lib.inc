%define EXIT 60
%define STOP_SYMB 0
%define SYSCALL_OUT 1
%define SYSCALL_IN 0
%define STDIN 1
%define STDOUT 1
%define ONE_SYMB 1
%define NEWLINE_SYMB 10
%define TAB_SYMB 9
%define ULONG_LENGTH 21

section .data
    buffer: db 0
section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT
    syscall
    
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    ;строка в rdi
    xor rax, rax
.loop:
    cmp byte [rdi + rax], STOP_SYMB ;сравниваем 
    je .done
    inc rax
    jmp .loop
.done:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, SYSCALL_OUT
    mov rdi, STDOUT
    syscall
    
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE_SYMB
; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, SYSCALL_OUT              ; Номер системного вызова для sys_write (1 - stdout)
    mov rdi, STDOUT             ; Файловый дескриптор stdout
    mov rdx, ONE_SYMB              ; Длина вывода (1 символ)
    syscall                 ; Вызываем sys_write

    pop rdi
    xor rax, rax
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    ; data in rdi
    xor rcx, rcx 
    mov r8, 10
    mov rax, rdi    ; TODO: убрать делитель или оставить
    enter ULONG_LENGTH, 0  ; освобождаем место в стеке, энтер грузит указатель стека в rdb 
    mov r9, rbp
    dec r9
    mov [rbp], byte 0   ; стоп символ на конец
.loop:                  ; делим на 10, пушим до тех пор пока не доделились
    dec r9
    xor rdx, rdx
    div r8
    add rdx, '0'        ; добавляем чтобы был символ
    mov byte [r9], dl   ; грузим в стек
    test rax, rax
    jnz .loop
    mov rdi, r9         ; аргумент для принтстринг
    call print_string

    leave
    xor rax, rax
    ret

; Выводит знаковое 8-байтовое число в десятичном формате  
print_int:
push r14
    mov r14, rdi    ; если минус то печатаем и домножаем на -1
    test r14, r14
    jns .done
    mov rdi, '-'
    call print_char
    neg r14
.done:
    mov rdi, r14
    call print_uint
    pop r14
    xor rax, rax
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    ; rdi, rsi
    ; побитовое сравнение, должно работать по идее, но сначала проверим на совпадение длин
    push r14
    push r13
    push r12
    mov r12, rdi         ; 1 str
    mov r13, rsi         ; 2 str
    call string_length   ; длина 1
    mov r14, rax
    mov rdi, r13
    call string_length    ; длина 2
    cmp rax, r14            ; сравнить длины
    jne .not_eq             ; если не равны то break
    test rax, rax
    jz .eq
    mov rcx, rax
    xor r14, r14
.loop:   ; в r14, rax лежат чары, rdi rsi указывают на память, в rcx счетчик
    dec rcx
    mov al, byte [r13 + rcx]
    mov r14b, byte [r12 + rcx]
    cmp al, r14b                    ; сравнение байтов
    jne .not_eq                     ; если не равны то сразу break
    test rcx, rcx
    jnz .loop
.eq:
    mov rax, 1
    jmp .end
.not_eq:
    xor rax, rax
.end:
    pop r12
    pop r13
    pop r14
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax      
    xor rdi, rdi      
    mov rsi, buffer   
    mov rdx, ONE_SYMB       
    syscall           

    cmp rax, 1      ; проверка на ошибку вывода
    js .error

    movzx rax, byte [buffer]
    ret 
.error:
    xor rax, rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14

    mov r12, rdi    ; buff address
    mov r13, rsi    ; buff length
    xor r14, r14    ; word length
.loop_skip:         ; Тут бы по хорошему убрать даблкод, но это его сильно усложнит, так что пусть все будет так как есть
    call read_char  ; проверка на пробельные
    cmp al, ' '
    je .loop_skip
    cmp al, TAB_SYMB
    je .loop_skip
    cmp al, NEWLINE_SYMB
    je .loop_skip

    mov byte [r12+r14], al  ; запись в буфер
    inc r14
    cmp al, 0
    je .done
    dec r13

.loop_read_char:
    call read_char
    mov byte [r12+r14], al
    inc r14
    cmp al, ' '            ; аналогичная первой части проверка на пробельные
    je .done
    cmp al, TAB_SYMB
    je .done
    cmp al, NEWLINE_SYMB
    je .done
    cmp al, 0
    je .done
    dec r13
    test r13,r13            ; проверка на то то буфер кончился
    jz .overflow
    jmp .loop_read_char

.overflow:                  ; возврат 0 в rax
    mov r12, 0
    jmp .end
.done:
    dec r14
    mov byte [r12+r14], 0
.end:                       ; конец)))
    mov rax, r12
    mov rdx, r14
    pop r14
    pop r13
    pop r12
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    ; rdi
    push rbx
    xor rax, rax            ; будем хранить число
    xor rcx, rcx            ; считает длину 
    mov rbx, 10             ; mul

.loop_read:
    movzx r10, byte [rdi+rcx]
    cmp r10, '0'           ; проверяем что это символ числа
    jb .done
    cmp r10, '9'   
    ja .done
    cmp r10, 0
    je .done
    sub r10, '0'           ; приводим к числу
    mul rbx                 ; умножаем  на 10
    add rax, r10            ; добавляем число
    
    inc rcx

    jmp .loop_read
.done:
    mov rdx, rcx
    pop rbx
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    ; пытаемся распарсить минус первый
    movzx r8, byte [rdi]
    cmp r8, '-'
    jne parse_uint
    ; negative
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    ; rdi - строка, rsi - буфер, rdx - длина
    cmp rdx, 0
    je .overflow        ; если буфер пустой то делать нам тут нечего
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    ja .overflow
    add rsi, rdx        ; чтобы не добавлять счетчик и не инвертить строку надо уменьшать rdx в цикле
    add rdi, rdx
.loop_copy:
    mov r9, rdi
    sub r9, rdx
    mov r8, [r9] ; используем r8 как tmp
    mov r9, rsi
    sub r9, rdx
    mov [r9], r8
    dec rdx
    test rdx, rdx
    jz .done
    jmp .loop_copy

.overflow:
    xor rax, rax
.done:    
    ret
